$(".w-banner .tab-content").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: ".w-banner .nav-pills",
  prevArrow:
    '<button type="button" class="slick-prev "><i class="bi bi-chevron-left"></i></button>',
  nextArrow:
    '<button type="button" class="slick-next "><i class="bi bi-chevron-right"></i></button>',
});
$(".w-banner .nav-pills").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  asNavFor: ".w-banner .tab-content",
  focusOnSelect: true,
});
$(".listProductOutstanding").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 5,
  prevArrow: '<button type="button" class="slick-prev d-none "></button>',
  nextArrow: '<button type="button" class="slick-next d-none"></button>',
});
$(".listFeedback").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  prevArrow: '<button type="button" class="slick-prev d-none "></button>',
  nextArrow: '<button type="button" class="slick-next d-none"></button>',
});
